-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:      -40.4728000371
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons         4.9999983938 
   Number of Alpha Electrons         4.9999983938 
   Total nuber of  Electrons         9.9999967876 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy      -5.0902871118 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0000024502
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:      -40.4728048154
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons         4.9999983937 
   Number of Alpha Electrons         4.9999983937 
   Total nuber of  Electrons         9.9999967874 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy      -5.0888869945 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0000024745
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:      -40.4728048760
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons         4.9999983945 
   Number of Alpha Electrons         4.9999983945 
   Total nuber of  Electrons         9.9999967890 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy      -5.0888728386 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0000024748
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 3
   prop. index: 1
       Filename                          : methane_M06-D3ZERO_6-31G_d_.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0013604311
        Electronic Contribution:
                  0    
      0       0.000615
      1      -0.000114
      2       0.000968
        Nuclear Contribution:
                  0    
      0      -0.000823
      1       0.000318
      2      -0.000519
        Total Dipole moment:
                  0    
      0      -0.000208
      1       0.000204
      2       0.000449
# -----------------------------------------------------------
$ Hessian
   description: Details about the Hessian
   geom. index: 3
   prop. index: 1
Normal modes:
Number of Rows: 15 Number of Columns: 15
                  0          1          2          3          4          5    
      0       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      1       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      2       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      3       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      4       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      5       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      6       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      7       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      8       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      9       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     10       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     11       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     12       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     13       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     14       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
                  6          7          8          9         10         11    
      0       0.027692   0.083039   0.088109  -0.000231  -0.000598   0.000912
      1      -0.056889  -0.070901   0.084486  -0.001552  -0.000934   0.000473
      2      -0.106806   0.059448  -0.021974   0.002122   0.002040   0.000570
      3       0.022235   0.064036   0.068983  -0.000209  -0.000699  -0.509918
      4       0.270489   0.353681  -0.420220  -0.486253  -0.068277  -0.000353
      5       0.525640  -0.291929   0.113201  -0.083102   0.484428   0.000565
      6       0.099544  -0.400895  -0.391035  -0.240044   0.406105   0.168701
      7       0.038512   0.332819  -0.439281   0.377822   0.297043  -0.177908
      8      -0.088677  -0.291009   0.011116  -0.244591   0.036998  -0.438817
      9      -0.238699  -0.469135  -0.221859   0.469487   0.005302   0.165064
     10      -0.028109   0.184424   0.144553  -0.165970   0.067185   0.459478
     11       0.469971  -0.305252   0.177752  -0.030016  -0.505976   0.063638
     12      -0.213046  -0.183468  -0.505968  -0.226476  -0.403586   0.165284
     13       0.396978  -0.026089  -0.291759   0.292893  -0.284818  -0.286854
     14       0.365730   0.179828  -0.040238   0.332426  -0.039762   0.367819
                 12         13         14    
      0      -0.082626  -0.042080  -0.005564
      1       0.027118  -0.043289  -0.077728
      2      -0.032206   0.070843  -0.050684
      3       0.761900   0.386383   0.047715
      4       0.004663  -0.007340  -0.013319
      5      -0.005865   0.011998  -0.009033
      6       0.015749   0.185117  -0.220767
      7      -0.027349  -0.209794   0.220543
      8      -0.084927  -0.487860   0.564530
      9       0.007803   0.136754   0.256075
     10       0.067687   0.391749   0.704715
     11       0.002485   0.067221   0.090917
     12       0.199094  -0.206837  -0.016724
     13      -0.368126   0.341209   0.014245
     14       0.472060  -0.435506  -0.042482
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 3
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         16.0430000000
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :        -40.4728073507
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0000358739
        Number of frequencies          :     15      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     1333.366964
      7     1343.898953
      8     1350.698296
      9     1556.790797
     10     1564.741426
     11     3039.050180
     12     3169.663961
     13     3173.080653
     14     3174.833648
        Zero Point Energy (Hartree)    :          0.0448938558
        Inner Energy (Hartree)         :        -40.4250450782
        Enthalpy (Hartree)             :        -40.4241008692
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0071638566
        Vibrational entropy            :          0.0000412527
        Translational entropy          :          0.0071638566
        Entropy                        :          0.0234847198
        Gibbs Energy (Hartree)         :        -40.4475855890
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.857070000000    0.342760000000    0.000000000000
               1 H      0.235130000000    0.342760000000   -0.000000000000
               2 H     -1.221130000000    0.729800000000    0.954230000000
               3 H     -1.221130000000   -0.677150000000   -0.141920000000
               4 H     -1.221130000000    0.975630000000   -0.812300000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.856921485564    0.342704089509    0.000093192276
               1 H      0.236597232444    0.342926310079   -0.000225783633
               2 H     -1.221791504049    0.730146224928    0.955379625635
               3 H     -1.221784519132   -0.678326852983   -0.141987192840
               4 H     -1.221429723699    0.976350228467   -0.813249841438
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     3 
    Coordinates:
               0 C     -0.856831599984    0.342669594316    0.000149704605
               1 H      0.236686742816    0.343025335310   -0.000355964517
               2 H     -1.221920658043    0.730084219786    0.955381054449
               3 H     -1.221913654410   -0.678303314393   -0.141934186026
               4 H     -1.221350830379    0.976324164981   -0.813230608512
