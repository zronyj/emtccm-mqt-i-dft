-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:      -40.0947790614
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons         4.9999983575 
   Number of Alpha Electrons         4.9999983575 
   Total nuber of  Electrons         9.9999967150 
   Exchange energy                  -5.8698323733 
   Correlation energy               -0.5924282396 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy      -6.4622606129 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:      -40.0953737663
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons         4.9999983240 
   Number of Alpha Electrons         4.9999983240 
   Total nuber of  Electrons         9.9999966480 
   Exchange energy                  -5.8502569737 
   Correlation energy               -0.5912044027 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy      -6.4414613763 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:      -40.0953740482
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons         4.9999983227 
   Number of Alpha Electrons         4.9999983227 
   Total nuber of  Electrons         9.9999966453 
   Exchange energy                  -5.8498380865 
   Correlation energy               -0.5911780899 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy      -6.4410161764 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 3
   prop. index: 1
       Filename                          : methane_VWN5_cc-pVDZ.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000039692
        Electronic Contribution:
                  0    
      0      -0.000003
      1       0.000005
      2       0.000003
        Nuclear Contribution:
                  0    
      0       0.000005
      1      -0.000005
      2      -0.000004
        Total Dipole moment:
                  0    
      0       0.000001
      1       0.000000
      2      -0.000001
# -----------------------------------------------------------
$ Hessian
   description: Details about the Hessian
   geom. index: 3
   prop. index: 1
Normal modes:
Number of Rows: 15 Number of Columns: 15
                  0          1          2          3          4          5    
      0       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      1       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      2       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      3       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      4       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      5       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      6       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      7       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      8       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      9       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     10       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     11       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     12       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     13       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     14       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
                  6          7          8          9         10         11    
      0       0.018391  -0.024343  -0.120703   0.000006  -0.000029  -0.000119
      1      -0.080078  -0.095079   0.006976   0.000008  -0.000038  -0.000024
      2      -0.093540   0.076608  -0.029702  -0.000001  -0.000038  -0.000023
      3       0.014094  -0.018635  -0.092255   0.000061  -0.000000   0.501020
      4       0.388507   0.461246  -0.033725   0.465674  -0.181693   0.000000
      5       0.453892  -0.371537   0.144183  -0.181857  -0.465589  -0.000058
      6       0.128504   0.040761   0.554252   0.006095  -0.471186  -0.166671
      7       0.157150   0.543346  -0.000527  -0.462554  -0.068583   0.177198
      8      -0.116329  -0.168748   0.226281   0.189951  -0.151966   0.436818
      9      -0.240550  -0.044707   0.515021  -0.411286   0.230402  -0.166450
     10      -0.035150   0.005404  -0.230806   0.110456  -0.140802  -0.466378
     11       0.394604  -0.434988   0.116557   0.262065   0.420992  -0.064926
     12      -0.121186   0.312648   0.461243   0.405054   0.241125  -0.166481
     13       0.443683   0.122931   0.181936  -0.113670   0.391533   0.289470
     14       0.382428   0.062434  -0.133100  -0.270142   0.197019  -0.371554
                 12         13         14    
      0       0.089263  -0.024251  -0.003411
      1      -0.022928  -0.078235  -0.043819
      2       0.008596   0.043100  -0.081458
      3      -0.830632   0.225616   0.031492
      4      -0.004294  -0.014769  -0.008232
      5       0.001597   0.008122  -0.015274
      6      -0.079623   0.052566  -0.271469
      7       0.098120  -0.075379   0.279775
      8       0.254204  -0.141580   0.694656
      9      -0.013406   0.234799   0.166148
     10      -0.088810   0.655666   0.458976
     11      -0.010110   0.101353   0.049663
     12      -0.139961  -0.224009   0.114478
     13       0.268190   0.366710  -0.208386
     14      -0.348125  -0.481456   0.241587
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 3
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         16.0430000000
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :        -40.0953740482
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0000570819
        Number of frequencies          :     15      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     1225.453377
      7     1225.653827
      8     1225.983066
      9     1457.776348
     10     1457.867598
     11     2965.412249
     12     3111.378249
     13     3111.591114
     14     3111.901748
        Zero Point Energy (Hartree)    :          0.0430414611
        Inner Energy (Hartree)         :        -40.0494429624
        Enthalpy (Hartree)             :        -40.0484987534
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0048540896
        Vibrational entropy            :          0.0000664256
        Translational entropy          :          0.0048540896
        Entropy                        :          0.0212001258
        Gibbs Energy (Hartree)         :        -40.0696988792
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.857070000000    0.342760000000    0.000000000000
               1 H      0.235130000000    0.342760000000   -0.000000000000
               2 H     -1.221130000000    0.729800000000    0.954230000000
               3 H     -1.221130000000   -0.677150000000   -0.141920000000
               4 H     -1.221130000000    0.975630000000   -0.812300000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.857068233426    0.342760997057    0.000001826504
               1 H      0.250316994861    0.342758716906   -0.000000576183
               2 H     -1.226193360922    0.735183742115    0.967499714255
               3 H     -1.226191944061   -0.691331637417   -0.143896130317
               4 H     -1.226193456453    0.984428181338   -0.823594834259
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     3 
    Coordinates:
               0 C     -0.857067316626    0.342761440823    0.000003089779
               1 H      0.250646509916    0.342757882777   -0.000000849273
               2 H     -1.226303723160    0.735300893195    0.967786958491
               3 H     -1.226301418346   -0.691637171834   -0.143940328304
               4 H     -1.226304051784    0.984616955039   -0.823838870694
