-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:      -79.7305734041
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons         8.9999964638 
   Number of Alpha Electrons         8.9999964638 
   Total nuber of  Electrons        17.9999929275 
   Exchange energy                 -12.4087076850 
   Correlation energy               -0.5607353762 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -12.9694430612 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:      -79.7310571645
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons         8.9999961821 
   Number of Alpha Electrons         8.9999961821 
   Total nuber of  Electrons        17.9999923643 
   Exchange energy                 -12.3913115516 
   Correlation energy               -0.5596400782 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -12.9509516298 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:      -79.7310923121
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons         8.9999959814 
   Number of Alpha Electrons         8.9999959814 
   Total nuber of  Electrons        17.9999919627 
   Exchange energy                 -12.3891138327 
   Correlation energy               -0.5594819462 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -12.9485957789 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:      -79.7311004420
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons         8.9999958422 
   Number of Alpha Electrons         8.9999958422 
   Total nuber of  Electrons        17.9999916844 
   Exchange energy                 -12.3892374740 
   Correlation energy               -0.5594961884 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -12.9487336624 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:      -79.7311027512
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons         8.9999957586 
   Number of Alpha Electrons         8.9999957586 
   Total nuber of  Electrons        17.9999915172 
   Exchange energy                 -12.3897692365 
   Correlation energy               -0.5595455625 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -12.9493147990 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:      -79.7311028977
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons         8.9999957497 
   Number of Alpha Electrons         8.9999957497 
   Total nuber of  Electrons        17.9999914994 
   Exchange energy                 -12.3899391178 
   Correlation energy               -0.5595620626 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -12.9495011804 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 6
   prop. index: 1
       Filename                          : ethane_PBE_def2-TZVP.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000296
        Electronic Contribution:
                  0    
      0      -0.000000
      1       0.000000
      2       0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2      -0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1       0.000000
      2       0.000000
# -----------------------------------------------------------
$ Hessian
   description: Details about the Hessian
   geom. index: 6
   prop. index: 1
Normal modes:
Number of Rows: 24 Number of Columns: 24
                  0          1          2          3          4          5    
      0       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      1       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      2       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      3       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      4       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      5       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      6       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      7       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      8       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      9       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     10       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     11       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     12       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     13       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     14       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     15       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     16       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     17       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     18       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     19       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     20       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     21       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     22       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     23       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
                  6          7          8          9         10         11    
      0      -0.000002  -0.000002   0.000005  -0.321527   0.000010  -0.000005
      1      -0.000010   0.030902  -0.037147   0.000002  -0.095473   0.111132
      2       0.000001  -0.037148  -0.030902   0.000005   0.111134   0.095474
      3      -0.000002  -0.000002   0.000005   0.321527  -0.000010   0.000005
      4      -0.000010   0.030902  -0.037147  -0.000002   0.095473  -0.111132
      5       0.000001  -0.037148  -0.030902  -0.000005  -0.111134  -0.095474
      6       0.000025   0.238672   0.445701  -0.363422  -0.224606  -0.435904
      7      -0.378275  -0.157776   0.151318  -0.004719   0.215920  -0.135654
      8       0.153447   0.143840   0.087720  -0.011616  -0.116165  -0.000408
      9      -0.000073   0.266679  -0.429576  -0.363369  -0.265284   0.412531
     10       0.056312  -0.108483   0.115362   0.012400   0.062780  -0.027722
     11      -0.404360   0.179659   0.137057   0.001753  -0.224131  -0.153626
     12       0.000070  -0.505332  -0.016186  -0.363369   0.489809   0.023492
     13       0.322078  -0.101963   0.175950  -0.007685   0.045820  -0.214341
     14       0.250898   0.119150   0.143439   0.009897  -0.037497  -0.170514
     15       0.000070  -0.505332  -0.016186   0.363369  -0.489809  -0.023492
     16       0.322078  -0.101963   0.175950   0.007685  -0.045820   0.214341
     17       0.250898   0.119150   0.143439  -0.009897   0.037497   0.170514
     18      -0.000073   0.266679  -0.429576   0.363369   0.265284  -0.412531
     19       0.056312  -0.108483   0.115362  -0.012400  -0.062780   0.027722
     20      -0.404360   0.179659   0.137057  -0.001753   0.224131   0.153626
     21       0.000025   0.238672   0.445701   0.363422   0.224606   0.435904
     22      -0.378275  -0.157776   0.151318   0.004719  -0.215920   0.135654
     23       0.153447   0.143840   0.087720   0.011616   0.116165   0.000408
                 12         13         14         15         16         17    
      0       0.092961  -0.105573  -0.000001  -0.000003  -0.000022   0.000014
      1      -0.000001  -0.000016   0.007198   0.017653   0.013736   0.047239
      2      -0.000014   0.000002  -0.017648   0.007193  -0.047241   0.013737
      3       0.092961   0.105573   0.000001   0.000003  -0.000022   0.000014
      4      -0.000001   0.000016  -0.007198  -0.017653   0.013736   0.047239
      5      -0.000014  -0.000002   0.017648  -0.007193  -0.047241   0.013737
      6      -0.369322   0.365120  -0.214432   0.208918  -0.180439   0.142463
      7      -0.062328   0.064784  -0.341508  -0.287336  -0.331107  -0.344937
      8      -0.153564   0.159598   0.047896   0.204890   0.030377   0.221985
      9      -0.369174   0.365037  -0.073677  -0.290160  -0.033063  -0.227635
     10       0.164072  -0.170587  -0.035409   0.128860  -0.052526   0.130547
     11       0.022968  -0.023774   0.464049  -0.101220   0.505786  -0.056604
     12      -0.369201   0.365092   0.288200   0.081213   0.213765   0.085008
     13      -0.101726   0.105859   0.171951  -0.344330   0.219964  -0.348497
     14       0.130765  -0.135857  -0.009119  -0.308623   0.026749  -0.329064
     15      -0.369200  -0.365093  -0.288200  -0.081212   0.213765   0.085008
     16      -0.101726  -0.105859  -0.171951   0.344328   0.219964  -0.348498
     17       0.130765   0.135857   0.009119   0.308622   0.026749  -0.329065
     18      -0.369174  -0.365037   0.073677   0.290159  -0.033063  -0.227636
     19       0.164071   0.170587   0.035409  -0.128859  -0.052526   0.130548
     20       0.022968   0.023774  -0.464049   0.101220   0.505786  -0.056605
     21      -0.369322  -0.365120   0.214432  -0.208918  -0.180439   0.142464
     22      -0.062328  -0.064784   0.341509   0.287335  -0.331107  -0.344938
     23      -0.153564  -0.159598  -0.047896  -0.204890   0.030377   0.221985
                 18         19         20         21         22         23    
      0       0.035929  -0.034644   0.000027  -0.000010   0.000022  -0.000018
      1      -0.000028   0.000020   0.054539   0.034802   0.058038   0.030295
      2       0.000039  -0.000047  -0.034798   0.054539  -0.030293   0.058036
      3      -0.035929  -0.034644  -0.000027   0.000010   0.000022  -0.000018
      4       0.000028   0.000020  -0.054539  -0.034802   0.058038   0.030295
      5      -0.000039  -0.000047   0.034798  -0.054539  -0.030293   0.058036
      6      -0.139408   0.137504  -0.037611   0.203150  -0.020058   0.207884
      7       0.143911  -0.144152   0.053331  -0.195210   0.033458  -0.199056
      8       0.354830  -0.355420   0.083755  -0.489989   0.041918  -0.494603
      9      -0.139401   0.137551  -0.157256  -0.134007  -0.170098  -0.121126
     10      -0.379286   0.380076  -0.402728  -0.346470  -0.430806  -0.309351
     11      -0.052755   0.052877  -0.068010  -0.034188  -0.068947  -0.030430
     12      -0.139628   0.137754   0.194562  -0.069022   0.189890  -0.086549
     13       0.235716  -0.236164  -0.305613   0.123709  -0.294212   0.147427
     14      -0.302531   0.303099   0.402171  -0.130810   0.387989  -0.166503
     15       0.139629   0.137753  -0.194562   0.069022   0.189890  -0.086549
     16      -0.235717  -0.236163   0.305613  -0.123709  -0.294212   0.147427
     17       0.302532   0.303097  -0.402172   0.130810   0.387989  -0.166503
     18       0.139402   0.137550   0.157256   0.134007  -0.170098  -0.121126
     19       0.379289   0.380073   0.402729   0.346471  -0.430806  -0.309351
     20       0.052755   0.052876   0.068010   0.034188  -0.068947  -0.030430
     21       0.139409   0.137503   0.037611  -0.203150  -0.020058   0.207883
     22      -0.143912  -0.144151  -0.053331   0.195210   0.033458  -0.199055
     23      -0.354832  -0.355418  -0.083755   0.489990   0.041918  -0.494602
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 6
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         30.0700000000
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :        -79.7311028977
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0006963067
        Number of frequencies          :     24      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     297.665110
      7     799.696988
      8     799.794443
      9     990.436243
     10     1177.448623
     11     1177.475536
     12     1357.081024
     13     1368.454095
     14     1451.482689
     15     1451.541830
     16     1453.303710
     17     1453.327684
     18     2968.918609
     19     2971.416615
     20     3021.533000
     21     3021.975000
     22     3047.062899
     23     3047.402097
        Zero Point Energy (Hartree)    :          0.0725733449
        Inner Energy (Hartree)         :        -79.6550007033
        Enthalpy (Hartree)             :        -79.6540564942
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0077448853
        Vibrational entropy            :          0.0010155850
        Translational entropy          :          0.0077448853
        Entropy                        :          0.0259298609
        Gibbs Energy (Hartree)         :        -79.6799863552
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     1 
    Coordinates:
               0 C     -1.069770000000    0.342760000000    0.000000000000
               1 C      0.442290000000    0.342760000000   -0.000000000000
               2 H     -1.454140000000    0.727780000000    0.949230000000
               3 H     -1.454140000000   -0.671810000000   -0.141180000000
               4 H     -1.454140000000    0.972310000000   -0.808050000000
               5 H      0.826660000000   -0.286790000000    0.808050000000
               6 H      0.826660000000    1.357330000000    0.141180000000
               7 H      0.826660000000   -0.042260000000   -0.949230000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     2 
    Coordinates:
               0 C     -1.077916586898    0.342756977434    0.000001893137
               1 C      0.450436584737    0.342763022248   -0.000001891788
               2 H     -1.472210263674    0.728475934198    0.950917734470
               3 H     -1.472208487461   -0.673622895420   -0.141438162938
               4 H     -1.472218583675    0.973435141587   -0.809486505379
               5 H      0.844738591096   -0.287915138292    0.809486507622
               6 H      0.844728484025    1.359142898889    0.141438157576
               7 H      0.844730261850   -0.042955940643   -0.950917732699
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     3 
    Coordinates:
               0 C     -1.079207620459    0.342755809316    0.000004368276
               1 C      0.451727614649    0.342764190243   -0.000004365647
               2 H     -1.477794888705    0.728054831068    0.949860286144
               3 H     -1.477787606460   -0.672495074690   -0.141285868320
               4 H     -1.477804558382    0.972734806114   -0.808583298868
               5 H      0.850324576967   -0.287214797361    0.808583301495
               6 H      0.850307600599    1.358015075345    0.141285860436
               7 H      0.850314881791   -0.042534840035   -0.949860283516
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     4 
    Coordinates:
               0 C     -1.078781037688    0.342755419020    0.000006684908
               1 C      0.451301030554    0.342764579610   -0.000006680271
               2 H     -1.479742456460    0.727724992992    0.949034953283
               3 H     -1.479730986403   -0.671613587400   -0.141166573395
               4 H     -1.479752286604    0.972187188814   -0.807878413414
               5 H      0.852272310226   -0.286667182209    0.807878413592
               6 H      0.852250982118    1.357133587951    0.141166566294
               7 H      0.852262444258   -0.042204998777   -0.949034950998
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     5 
    Coordinates:
               0 C     -1.077996392016    0.342755482924    0.000008421692
               1 C      0.450516385301    0.342764516666   -0.000008416931
               2 H     -1.480158623668    0.727549974650    0.948597796561
               3 H     -1.480145021569   -0.671145887849   -0.141103640446
               4 H     -1.480168289157    0.971896244137   -0.807504966588
               5 H      0.852688307633   -0.286376233643    0.807504968540
               6 H      0.852665023024    1.356665887424    0.141103632449
               7 H      0.852678610452   -0.042029984309   -0.948597795276
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     6 
    Coordinates:
               0 C     -1.077751911636    0.342755694928    0.000008772934
               1 C      0.450271905314    0.342764304962   -0.000008768687
               2 H     -1.479957334684    0.727545866487    0.948588384730
               3 H     -1.479943951715   -0.671135137799   -0.141102468607
               4 H     -1.479966927332    0.971889230297   -0.807496932168
               5 H      0.852486944272   -0.286369223448    0.807496933749
               6 H      0.852463954206    1.356655137828    0.141102463556
               7 H      0.852477321575   -0.042025873257   -0.948588385507
