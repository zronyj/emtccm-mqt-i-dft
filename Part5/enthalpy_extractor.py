import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

here = os.getcwd()

files = os.listdir(here)

paths = [os.path.join(here, f) for f in files if '.' not in f]

data = {}
functionals = []
basis_sets = []

for path in paths:
	temp_files = os.listdir(path)
	temp_names = [tf[:-4] for tf in temp_files if '.out' in tf]
	for tp in temp_names:
		pieces = tp.split('_')
		name = pieces[0]
		functional = pieces[1]
		basis = pieces[2]
		functionals.append(functional)
		basis_sets.append(basis)
		with open(os.path.join(path, tp + '.out'), 'r') as out:
			extracted = out.readlines()
		for line in extracted:
			if 'Total Enthalpy' in line:
				out_pieces = line.split()
				out_enthalpy = out_pieces[3]
				break
		if not name in data.keys():
			data[name] = {}
		if not functional in data[name].keys():
			data[name][functional] = {}
		if not basis in data[name][functional].keys():
			data[name][functional][basis] = {}
		data[name][functional][basis] = float(out_enthalpy)

functionals = list(set(functionals))
basis_sets = list(set(basis_sets))

print("\n\n")

for i, func in enumerate(functionals):
	print(f"{i}. {func}", end="   ")

str_fun = input('\n\nPlease select the order of the functionals for the graph: ')

print("\n\n")

for i, basi in enumerate(basis_sets):
	print(f"{i}. {basi}", end="   ")

str_bas = input('\n\nPlease select the order of the basis sets for the graph: ')

functionals = [functionals[int(i)] for i in str_fun.split()]
basis_sets = [basis_sets[int(j)] for j in str_bas.split()]

new_basis_set = []

for b in basis_sets:
	if b == "6-31G":
		new_basis_set.append("6-31G(d)")
	elif b == "6-311G":
		new_basis_set.append("6-311G(d,p)")
	else:
		new_basis_set.append(b)

reacts = {"propane": 1, "butane": 2, "pentane": 3, "hexane": 4}

reactions = {}
dataframes = {}

for alkane, m in reacts.items():
	reactions[alkane] = []
	for fun in functionals:
		temp2 = []
		for bas in basis_sets:
			react_enthalpy = (m + 1) * data["ethane"][fun][bas] - ( data[alkane][fun][bas] + m * data["methane"][fun][bas] )
			temp2.append(react_enthalpy * 2625.5)
		reactions[alkane].append(np.array(temp2))

	fig, ax = plt.subplots()
	im = ax.imshow(reactions[alkane])

	ax.set_xticks(np.arange(len(new_basis_set)), labels=new_basis_set)
	ax.set_yticks(np.arange(len(functionals)), labels=functionals)

	plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

	font = {'family': 'monospace', 'size': 5}

	for i in range(len(functionals)):
		for j in range(len(basis_sets)):
			text = ax.text(j, i, "{:.1f}".format(reactions[alkane][i][j]), ha="center", va="center", color="w", fontdict=font)

	title = [0]*3
	title[0] = "Benchmark: Enthalpies of the reaction"
	title[1] = r"$CH_{3}(CH_{2})_{" + str(m) + r"}CH_{3} + " + str(m) + r" CH_{4} \rightarrow " + str(m + 1) + r" C_{2}H_{6}$"
	title[2] = r"Values given in $kJ\ mol^{-1}$"

	ax.set_title("\n".join(title))
	fig.tight_layout()
	plt.savefig(f"{alkane}.png", dpi=300)
	plt.clf()

	dataframes[alkane] = pd.DataFrame(reactions[alkane], index=functionals, columns=basis_sets)
	dataframes[alkane].to_excel(f"{alkane}.xlsx")