import os
here = os.getcwd()

functionals = ["VWN5", "PBE", "PBE0", "BHLYP", "M06", "B3LYP", "RI-B2PLYP", "PBE D3", "BHLYP D3", "B3LYP D3", "M06 D3ZERO"]
basis_sets = ["STO-3G", "6-31G(d)", "6-311G(d,p)", "def2-TZVP", "cc-pVDZ", "aug-cc-pVTZ"]

molecules = {"methane":"""   C       -0.85707        0.34276        0.00000
   H        0.23513        0.34276       -0.00000
   H       -1.22113        0.72980        0.95423
   H       -1.22113       -0.67715       -0.14192
   H       -1.22113        0.97563       -0.81230""",
"ethane":"""   C       -1.06977        0.34276        0.00000
   C        0.44229        0.34276       -0.00000
   H       -1.45414        0.72778        0.94923
   H       -1.45414       -0.67181       -0.14118
   H       -1.45414        0.97231       -0.80805
   H        0.82666       -0.28679        0.80805
   H        0.82666        1.35733        0.14118
   H        0.82666       -0.04226       -0.94923""",
"propane":"""   C       -1.15214        0.23107       -0.01892
   C        0.36672        0.19320       -0.01765
   H       -1.53560        0.62392        0.92820
   H       -1.55655       -0.77648       -0.15744
   H       -1.52818        0.86268       -0.83014
   H        0.71446       -0.46687        0.78471
   C        0.96202        1.57801        0.17288
   H        0.72184       -0.22943       -0.96405
   H        0.64537        2.01344        1.12605
   H        0.65281        2.25217       -0.63229
   H        2.05520        1.52457        0.17025""",
"butane":"""   C       -1.26508        0.19385       -0.02543
   C        0.25520        0.17957       -0.02299
   H       -1.65535        0.57598        0.92326
   H       -1.65342       -0.81931       -0.16903
   H       -1.65021        0.82310       -0.83417
   H        0.60739       -0.48344        0.77585
   C        0.82770        1.58154        0.17584
   H        0.61250       -0.23769       -0.97168
   H        0.47041        1.99879        1.12454
   H        0.47548        2.24456       -0.62299
   C        2.34797        1.56726        0.17825
   H        2.73313        0.93799        0.98696
   H        2.73632        2.58042        0.32187
   H        2.73823        1.18516       -0.77046""",
"pentane":"""   C       -1.33320        0.14491       -0.03501
   C        0.18662        0.10359       -0.03278
   H       -1.71642        0.53617        0.91283
   H       -1.73952       -0.86152       -0.17614
   H       -1.70725        0.77899       -0.84518
   H        0.52695       -0.56356        0.76772
   C        0.78453        1.49659        0.16272
   H        0.53607       -0.32211       -0.98060
   H        0.42853        1.91976        1.10999
   H        0.43770        2.16114       -0.63805
   C        2.31229        1.45056        0.16438
   H        2.66422        0.79080        0.96599
   C        2.91119        2.83445        0.35857
   H        2.67339        1.03217       -0.78234
   H        2.59515        3.26837        1.31260
   H        2.60440        3.51106       -0.44543
   H        4.00423        2.77811        0.35655""",
"hexane":"""   C       -5.59672       -0.89590       -0.00708
   C       -4.34832       -0.02841       -0.02915
   H       -6.49392       -0.26980       -0.03794
   H       -5.63690       -1.50250        0.90327
   H       -5.62217       -1.56881       -0.87015
   C       -3.07679       -0.87559        0.01307
   H       -4.36973        0.65429        0.82827
   H       -4.35509        0.58836       -0.93538
   C       -1.82334        0.00071       -0.00926
   H       -3.06237       -1.55951       -0.84432
   H       -3.07703       -1.49360        0.91924
   C       -0.55182       -0.84647        0.03296
   H       -1.83776        0.68462        0.84815
   H       -1.82311        0.61873       -0.91542
   C        0.69658        0.02102        0.01093
   H       -0.53040       -1.52915       -0.82448
   H       -0.54506       -1.46328        0.93916
   H        0.73677        0.62766       -0.89940
   H        1.59378       -0.60508        0.04177
   H        0.72203        0.69389        0.87403"""}

inp = """# {name}
! {funct} {basis} Opt {freq}

* xyz 0 1
{coords}
*"""
for f in functionals:
	new_dir = os.path.join(here, f)
	if not os.path.exists(new_dir):
		os.makedirs(new_dir)
	os.chdir(new_dir)
	for b in basis_sets:
		for mk, mv in molecules.items():
			fun = f.replace(" ", "-")
			bas = b.replace("(", "_").replace(")", "_").replace(",", "-")
			if f == "RI-B2PLYP":
				fr = "NumFreq"
				baux = " AutoAux"
			else:
				fr = "Freq"
				baux = ""
			file_name = f"{mk}_{fun}_{bas}"
			with open(file_name + ".inp", "w") as entrada:
				entrada.write(inp.format(funct=f, basis=b + baux, coords=mv, name=mk, freq=fr))
			print(f"Running {file_name} ...", end=" ")
			os.system(f"orca.exe {file_name}.inp > {file_name}.out")
			print("finished!")
	os.chdir(here)