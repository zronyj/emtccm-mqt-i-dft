-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:      -40.4223485526
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons         4.9999997034 
   Number of Alpha Electrons         4.9999997034 
   Total nuber of  Electrons         9.9999994068 
   Exchange energy                  -3.0898013603 
   Correlation energy               -0.2148735036 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy      -3.3046748639 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ MP2_Energies
   description: The MP2 energies
   geom. index: 1
   prop. index: 1
        Reference Energy:        -40.4223485526
        Correlation Energy:       -0.0649451292
        Total MP2 Energy:        -40.4872936818
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:      -40.4225066797
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons         4.9999996618 
   Number of Alpha Electrons         4.9999996618 
   Total nuber of  Electrons         9.9999993235 
   Exchange energy                  -3.0933725170 
   Correlation energy               -0.2151228910 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy      -3.3084954080 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ MP2_Energies
   description: The MP2 energies
   geom. index: 2
   prop. index: 1
        Reference Energy:        -40.4225066797
        Correlation Energy:       -0.0648636919
        Total MP2 Energy:        -40.4873703716
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:      -40.4225085331
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons         4.9999996605 
   Number of Alpha Electrons         4.9999996605 
   Total nuber of  Electrons         9.9999993209 
   Exchange energy                  -3.0934532978 
   Correlation energy               -0.2151285833 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy      -3.3085818811 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ MP2_Energies
   description: The MP2 energies
   geom. index: 3
   prop. index: 1
        Reference Energy:        -40.4225085331
        Correlation Energy:       -0.0648618839
        Total MP2 Energy:        -40.4873704169
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 3
   prop. index: 1
       Filename                          : methane_RI-B2PLYP_aug-cc-pVTZ.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0003471603
        Electronic Contribution:
                  0    
      0       0.000076
      1      -0.000060
      2      -0.000107
        Nuclear Contribution:
                  0    
      0      -0.000001
      1      -0.000052
      2       0.000090
        Total Dipole moment:
                  0    
      0       0.000075
      1      -0.000113
      2      -0.000017
# -----------------------------------------------------------
$ MP2_Electric_Properties
   description: The MP2 Calculated Electric Properties
   geom. index: 3
   prop. index: 1
       Filename                          : methane_RI-B2PLYP_aug-cc-pVTZ.pmp2ur 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0003467395
        Electronic Contribution:
                  0    
      0       0.000076
      1      -0.000060
      2      -0.000106
        Nuclear Contribution:
                  0    
      0      -0.000001
      1      -0.000052
      2       0.000090
        Total Dipole moment:
                  0    
      0       0.000075
      1      -0.000113
      2      -0.000017
# -----------------------------------------------------------
$ MP2_Electric_Properties
   description: The MP2 Calculated Electric Properties
   geom. index: 3
   prop. index: 2
       Filename                          : methane_RI-B2PLYP_aug-cc-pVTZ.pmp2re 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0003430487
        Electronic Contribution:
                  0    
      0       0.000073
      1      -0.000060
      2      -0.000106
        Nuclear Contribution:
                  0    
      0      -0.000001
      1      -0.000052
      2       0.000090
        Total Dipole moment:
                  0    
      0       0.000072
      1      -0.000113
      2      -0.000017
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:      -40.4225085366
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons         4.9999996604 
   Number of Alpha Electrons         4.9999996604 
   Total nuber of  Electrons         9.9999993209 
   Exchange energy                  -3.0934532582 
   Correlation energy               -0.2151285878 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy      -3.3085818460 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ MP2_Energies
   description: The MP2 energies
   geom. index: 4
   prop. index: 1
        Reference Energy:        -40.4225085366
        Correlation Energy:       -0.0648618899
        Total MP2 Energy:        -40.4873704266
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 4
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         16.0430000000
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :        -40.4873704266
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0000343276
        Number of frequencies          :     15      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     1351.544356
      7     1352.345396
      8     1352.904006
      9     1575.011583
     10     1575.288244
     11     3049.984283
     12     3162.002186
     13     3162.551766
     14     3162.772605
        Zero Point Energy (Hartree)    :          0.0449810630
        Inner Energy (Hartree)         :        -40.4395224931
        Enthalpy (Hartree)             :        -40.4385782841
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0047997241
        Vibrational entropy            :          0.0000394376
        Translational entropy          :          0.0047997241
        Entropy                        :          0.0211187722
        Gibbs Energy (Hartree)         :        -40.4596970563
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.857070000000    0.342760000000    0.000000000000
               1 H      0.235130000000    0.342760000000   -0.000000000000
               2 H     -1.221130000000    0.729800000000    0.954230000000
               3 H     -1.221130000000   -0.677150000000   -0.141920000000
               4 H     -1.221130000000    0.975630000000   -0.812300000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.857067218997    0.342770319876   -0.000015570259
               1 H      0.229710175313    0.342754101974    0.000010407191
               2 H     -1.219319813717    0.727875513980    0.949494396750
               3 H     -1.219322065279   -0.672092300245   -0.141210340785
               4 H     -1.219331077319    0.972492364416   -0.808268892897
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     3 
    Coordinates:
               0 C     -0.857065754607    0.342774915028   -0.000023519578
               1 H      0.229588198091    0.342750707081    0.000017929657
               2 H     -1.219275603032    0.727826000742    0.949383788691
               3 H     -1.219280516133   -0.671976956238   -0.141188179207
               4 H     -1.219296324318    0.972425333386   -0.808180019562
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     4 
    Coordinates:
               0 C     -0.857065754607    0.342774915028   -0.000023519578
               1 H      0.229588198091    0.342750707081    0.000017929657
               2 H     -1.219275603032    0.727826000742    0.949383788691
               3 H     -1.219280516133   -0.671976956238   -0.141188179207
               4 H     -1.219296324318    0.972425333386   -0.808180019562
