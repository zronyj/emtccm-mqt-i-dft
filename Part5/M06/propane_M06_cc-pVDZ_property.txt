-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -119.0289824849
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons        13.0000256841 
   Number of Alpha Electrons        13.0000256841 
   Total nuber of  Electrons        26.0000513681 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -14.2077000336 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -119.0295259667
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons        13.0000246355 
   Number of Alpha Electrons        13.0000246355 
   Total nuber of  Electrons        26.0000492710 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -14.1896832798 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -119.0296125014
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons        13.0000237376 
   Number of Alpha Electrons        13.0000237376 
   Total nuber of  Electrons        26.0000474753 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -14.1879183819 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -119.0296388171
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons        13.0000229563 
   Number of Alpha Electrons        13.0000229563 
   Total nuber of  Electrons        26.0000459126 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -14.1893127715 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -119.0296404027
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons        13.0000227983 
   Number of Alpha Electrons        13.0000227983 
   Total nuber of  Electrons        26.0000455966 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -14.1900896878 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:     -119.0296405653
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons        13.0000227585 
   Number of Alpha Electrons        13.0000227585 
   Total nuber of  Electrons        26.0000455169 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -14.1902704010 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 6
   prop. index: 1
       Filename                          : propane_M06_cc-pVDZ.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0581202144
        Electronic Contribution:
                  0    
      0       0.272928
      1      -0.417582
      2      -0.055358
        Nuclear Contribution:
                  0    
      0      -0.260406
      1       0.398637
      2       0.052693
        Total Dipole moment:
                  0    
      0       0.012522
      1      -0.018946
      2      -0.002666
# -----------------------------------------------------------
$ Hessian
   description: Details about the Hessian
   geom. index: 6
   prop. index: 1
Normal modes:
Number of Rows: 33 Number of Columns: 33
                  0          1          2          3          4          5    
      0       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      1       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      2       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      3       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      4       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      5       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      6       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      7       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      8       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      9       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     10       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     11       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     12       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     13       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     14       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     15       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     16       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     17       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     18       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     19       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     20       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     21       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     22       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     23       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     24       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     25       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     26       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     27       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     28       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     29       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     30       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     31       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     32       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
                  6          7          8          9         10         11    
      0       0.000447  -0.008984  -0.111767   0.000243   0.024163  -0.171605
      1       0.007487  -0.012971  -0.137268   0.002784   0.009346  -0.016480
      2      -0.041283   0.024311  -0.023807  -0.014036  -0.064879  -0.019556
      3       0.000489  -0.007511  -0.091134   0.001112  -0.009440   0.085962
      4      -0.005487   0.023010   0.137845   0.011559   0.012567  -0.110497
      5       0.032178  -0.080835   0.030980  -0.078024  -0.005299  -0.014890
      6      -0.049503   0.061007  -0.280868   0.249112   0.413370  -0.076331
      7       0.236395   0.429420  -0.327748  -0.059999  -0.037617  -0.010017
      8      -0.158360  -0.133550  -0.012873   0.116210   0.118563   0.021236
      9       0.002040   0.021068   0.189428   0.009951   0.014441  -0.276995
     10      -0.022757  -0.098905  -0.261433  -0.020634  -0.025219   0.013573
     11       0.169640   0.542051  -0.112141   0.121450   0.219310   0.056986
     12       0.055192  -0.103608  -0.265309  -0.269758  -0.357259  -0.267686
     13      -0.186845  -0.426568  -0.208471   0.021343   0.028047   0.002595
     14      -0.224548  -0.264911  -0.007678   0.132142   0.138081   0.039330
     15      -0.023524   0.046009  -0.085441  -0.250741  -0.292755  -0.081873
     16       0.016572  -0.046989   0.131194   0.354197  -0.138872  -0.229805
     17       0.061967  -0.164641   0.025495   0.334751  -0.006378  -0.038289
     18      -0.000212   0.013298   0.169469   0.002998  -0.026729   0.156787
     19      -0.003364  -0.003176   0.047071   0.000974  -0.020605   0.096605
     20       0.014509   0.046846   0.001407  -0.019468   0.065794   0.030925
     21       0.021649  -0.056868  -0.064261   0.225651   0.316264   0.058789
     22      -0.033811   0.105210   0.111295  -0.423370   0.196413  -0.134278
     23       0.053503  -0.138417   0.051791   0.215230   0.036895  -0.017172
     24       0.401424  -0.154963   0.405130  -0.083840   0.151556  -0.160749
     25      -0.191350  -0.019434   0.156999  -0.304413   0.370943   0.081776
     26       0.245839  -0.004758   0.031955   0.096834  -0.061962  -0.070747
     27      -0.421912   0.215275   0.332663   0.066625  -0.077605  -0.212867
     28       0.127010   0.038286   0.134400   0.260588  -0.325091  -0.102078
     29       0.285409   0.004605   0.012635   0.177324  -0.175627  -0.001785
     30       0.005999   0.010182   0.167030   0.001136  -0.024941   0.169983
     31       0.071041  -0.062795  -0.303986  -0.010235  -0.084195   0.740129
     32      -0.497840   0.274947  -0.091411   0.134994  -0.216652   0.052396
                 12         13         14         15         16         17    
      0      -0.043123  -0.152249   0.045333  -0.002345   0.000734  -0.034700
      1      -0.096795   0.022485  -0.130334  -0.021247   0.006278   0.045270
      2      -0.010678   0.002099  -0.024273   0.117319  -0.075484   0.001046
      3       0.008278   0.186582  -0.109006   0.000623   0.007825  -0.118505
      4      -0.061377   0.118184   0.161213   0.033293   0.007753  -0.073788
      5      -0.011540   0.020141   0.031334  -0.176268   0.003339  -0.011158
      6       0.233983  -0.230258   0.348903  -0.319823   0.161015   0.196371
      7       0.209156   0.012722   0.214398   0.110870  -0.052031  -0.090472
      8      -0.023785  -0.018940  -0.041011  -0.078596   0.016744   0.139213
      9      -0.613941  -0.179155  -0.355130  -0.020869  -0.015418   0.293799
     10       0.154670   0.030787   0.053587   0.039572  -0.014371  -0.093907
     11       0.020822   0.004549   0.025742  -0.255920   0.124904   0.000150
     12       0.257888  -0.231347   0.314298   0.358086  -0.181361   0.174682
     13       0.182940   0.007230   0.200978  -0.078799   0.066889  -0.030352
     14       0.068020   0.020051   0.110733  -0.114564   0.061472  -0.143110
     15       0.217856   0.465065  -0.092663  -0.184846   0.526918   0.422327
     16       0.083669   0.288200   0.138216   0.311116   0.322190   0.279872
     17       0.018686   0.039373  -0.000028   0.150084   0.037012   0.044959
     18      -0.027598  -0.039590   0.097906   0.003111  -0.003441   0.018533
     19       0.092373  -0.143895  -0.090614  -0.023407  -0.010982  -0.067015
     20       0.009399  -0.023400  -0.017364   0.117673   0.071797  -0.005446
     21       0.206503   0.468594  -0.098557   0.170981  -0.558354   0.363497
     22       0.046357   0.307539   0.185047  -0.302290  -0.361629   0.237568
     23       0.014583   0.041480   0.028464   0.049322  -0.045955   0.032661
     24       0.231335  -0.098014  -0.313022   0.027771   0.012516   0.067209
     25       0.168467  -0.234131  -0.210844   0.337531   0.164733   0.270746
     26       0.065404  -0.007643  -0.101478  -0.055046  -0.007326  -0.133534
     27       0.238910  -0.088634  -0.304134  -0.055840  -0.005046   0.054311
     28       0.218212  -0.200875  -0.192812  -0.343695  -0.172106   0.204709
     29       0.013592  -0.044804   0.046266  -0.142468  -0.064057   0.191205
     30      -0.028480  -0.043611   0.092401   0.007980  -0.001256   0.032515
     31      -0.279430  -0.173028   0.323228   0.061062   0.009991   0.360170
     32      -0.024577  -0.020253   0.054089  -0.252562  -0.118641   0.053842
                 18         19         20         21         22         23    
      0       0.014499   0.150864   0.011494  -0.000773   0.020444  -0.000390
      1       0.036085   0.007576   0.005381  -0.008951  -0.026437  -0.000629
      2       0.002681   0.005170  -0.018038  -0.015806   0.001015  -0.035241
      3      -0.087595  -0.061072   0.000802  -0.016389  -0.063294   0.011394
      4      -0.068715  -0.027175  -0.032788   0.030008  -0.018104  -0.015109
      5      -0.009300  -0.004409   0.005056   0.006020  -0.007210  -0.033459
      6       0.020176  -0.464079  -0.122295  -0.184271  -0.152189  -0.324188
      7      -0.149780  -0.096812  -0.195234   0.041247   0.376409  -0.238307
      8       0.079750  -0.192507   0.018174  -0.101608  -0.233689  -0.053931
      9       0.024335  -0.455285  -0.065774   0.116129   0.236081   0.041720
     10       0.027450   0.264606  -0.002809  -0.087713  -0.101404  -0.102950
     11       0.007182  -0.002782   0.254877   0.232963  -0.051423   0.569820
     12       0.008521  -0.490686   0.110058   0.058176  -0.167634   0.264130
     13      -0.115231  -0.187701   0.070193   0.256071   0.253667   0.343205
     14      -0.112439   0.139467  -0.002568   0.174462   0.307816   0.128970
     15       0.248350   0.110111  -0.136269   0.125489   0.192290  -0.089446
     16       0.127970   0.117614   0.167467  -0.263428   0.000646   0.104243
     17       0.012243   0.034219   0.225727  -0.291932  -0.101614   0.105949
     18       0.085686   0.015394  -0.016371   0.011638  -0.016005   0.008750
     19       0.127469  -0.030568   0.003300  -0.011570   0.028927   0.004622
     20       0.022940  -0.004424   0.029113   0.023495  -0.003712  -0.017412
     21       0.224153   0.095707  -0.080423   0.208625   0.190409  -0.017605
     22       0.108298   0.117152   0.299835  -0.277127  -0.047928   0.068668
     23       0.025677  -0.003879  -0.167841   0.222224   0.099949  -0.084076
     24      -0.392880  -0.019268  -0.146344  -0.385924   0.279659   0.175897
     25      -0.337656   0.166509  -0.282828   0.091430  -0.265472   0.124599
     26       0.064651  -0.105199   0.102819  -0.171375   0.237055  -0.008382
     27      -0.346674  -0.045878   0.487244   0.121430   0.127167  -0.287539
     28      -0.306440   0.123195  -0.021441   0.222037  -0.243985  -0.059147
     29      -0.166946   0.143290  -0.193945   0.160587  -0.277359   0.048383
     30       0.064004   0.016008   0.002350   0.006171  -0.004493   0.001652
     31      -0.484680   0.093207   0.252064  -0.095561   0.214119  -0.107855
     32      -0.104585   0.031039  -0.429455  -0.388681   0.137314   0.319351
                 24         25         26         27         28         29    
      0      -0.016074   0.022659  -0.044462   0.013696   0.000614   0.001715
      1      -0.029627  -0.004547   0.011588   0.002897   0.003792   0.016126
      2       0.006747   0.000222  -0.000264   0.000197  -0.022337  -0.067326
      3       0.035777   0.000160   0.013720   0.035413  -0.002507   0.000158
      4      -0.065802  -0.005117  -0.019551  -0.055065  -0.008658   0.000091
      5       0.000030  -0.000426  -0.001159  -0.003436   0.085982  -0.002463
      6      -0.002349  -0.087264   0.173885  -0.049111  -0.060911  -0.176689
      7       0.428067   0.096241  -0.185697   0.055809   0.067261   0.191866
      8      -0.177588   0.231395  -0.450454   0.129375   0.153150   0.436502
      9       0.266510  -0.083038   0.154056  -0.070295  -0.004215  -0.028435
     10      -0.118766  -0.205927   0.366785  -0.176202  -0.008231  -0.063137
     11      -0.186239  -0.028490   0.050755  -0.024601  -0.007451  -0.022074
     12      -0.164024  -0.090747   0.181881  -0.047092   0.059481   0.184838
     13       0.184728   0.165181  -0.321950   0.087686  -0.105549  -0.321373
     14       0.238929  -0.205788   0.402984  -0.106384   0.126648   0.388845
     15      -0.131728  -0.010992  -0.078007  -0.217558  -0.204321   0.007390
     16       0.203494   0.027627   0.145376   0.414529   0.383978  -0.007853
     17       0.296558  -0.030661  -0.176510  -0.507508  -0.442882   0.011156
     18       0.025097   0.011092   0.003410  -0.007955  -0.000989   0.009023
     19       0.003449   0.045184   0.021802  -0.007094   0.002877  -0.010451
     20       0.006035   0.004087   0.001853  -0.001310  -0.022074   0.060158
     21      -0.168037  -0.009898  -0.071123  -0.200945   0.232155  -0.009028
     22       0.299068   0.016271   0.082155   0.235659  -0.277129   0.006494
     23      -0.237749   0.033276   0.188250   0.546019  -0.593659   0.016992
     24      -0.233106   0.165211   0.075119  -0.033579  -0.055803   0.139064
     25       0.152174  -0.205562  -0.096848   0.039835   0.069905  -0.177266
     26      -0.152244  -0.458539  -0.213265   0.087003   0.147559  -0.368900
     27      -0.118650   0.162363   0.074332  -0.029811   0.051975  -0.142987
     28       0.147124  -0.336757  -0.159536   0.057806  -0.107151   0.301966
     29       0.174403   0.416342   0.195115  -0.070461   0.127505  -0.358923
     30       0.017559  -0.449716  -0.184474   0.158012   0.015977  -0.103987
     31      -0.199889   0.019683   0.004817  -0.008966   0.000620   0.000604
     32      -0.108734  -0.003802  -0.001997   0.000759  -0.006201   0.011160
                 30         31         32    
      0       0.001603   0.003414   0.008544
      1       0.014333   0.039120   0.080337
      2      -0.055089   0.008286   0.020420
      3      -0.000438   0.001734  -0.004279
      4       0.005457  -0.002039   0.006335
      5      -0.033085  -0.002653   0.003725
      6      -0.146876   0.058265   0.134103
      7       0.155551  -0.056832  -0.125093
      8       0.355015  -0.148686  -0.330067
      9      -0.027456  -0.148172  -0.308910
     10      -0.061104  -0.339649  -0.705075
     11      -0.017307  -0.046882  -0.097305
     12       0.157301   0.043918   0.084155
     13      -0.266095  -0.071021  -0.129803
     14       0.323667   0.097423   0.182786
     15       0.082155  -0.002048   0.015052
     16      -0.155200   0.004717  -0.027646
     17       0.181233  -0.005138   0.031690
     18      -0.012609   0.077814  -0.036857
     19       0.012026  -0.025494   0.011270
     20      -0.062878  -0.017791   0.008939
     21      -0.078636  -0.012469   0.029023
     22       0.093973   0.016753  -0.034047
     23       0.203414   0.034734  -0.073442
     24      -0.140154  -0.123459   0.057930
     25       0.183639   0.171086  -0.083653
     26       0.376065   0.378277  -0.182066
     27       0.150799  -0.045274   0.018673
     28      -0.328332   0.119730  -0.054200
     29       0.386777  -0.154998   0.069474
     30       0.139241  -0.759303   0.358318
     31      -0.001544   0.017148  -0.007523
     32      -0.008972  -0.009865   0.004721
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 6
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         44.0970000000
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -119.0296405653
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0016631888
        Number of frequencies          :     33      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     219.695509
      7     316.441882
      8     373.214204
      9     727.294661
     10     887.643317
     11     898.154776
     12     916.031012
     13     1086.151513
     14     1162.886284
     15     1182.248795
     16     1291.274940
     17     1335.104657
     18     1377.278793
     19     1387.750776
     20     1429.859939
     21     1431.072227
     22     1440.661321
     23     1452.020100
     24     1458.077104
     25     3012.388395
     26     3014.557944
     27     3016.682599
     28     3059.315185
     29     3101.095807
     30     3107.084086
     31     3116.721932
     32     3118.285421
        Zero Point Energy (Hartree)    :          0.1023329962
        Inner Energy (Hartree)         :       -118.9228118375
        Enthalpy (Hartree)             :       -118.9218676284
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0107669054
        Vibrational entropy            :          0.0025592333
        Translational entropy          :          0.0107669054
        Entropy                        :          0.0310377683
        Gibbs Energy (Hartree)         :       -118.9529053968
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    11 
    Geometry Index:     1 
    Coordinates:
               0 C     -1.152140000000    0.231070000000   -0.018920000000
               1 C      0.366720000000    0.193200000000   -0.017650000000
               2 H     -1.535600000000    0.623920000000    0.928200000000
               3 H     -1.556550000000   -0.776480000000   -0.157440000000
               4 H     -1.528180000000    0.862680000000   -0.830140000000
               5 H      0.714460000000   -0.466870000000    0.784710000000
               6 C      0.962020000000    1.578010000000    0.172880000000
               7 H      0.721840000000   -0.229430000000   -0.964050000000
               8 H      0.645370000000    2.013440000000    1.126050000000
               9 H      0.652810000000    2.252170000000   -0.632290000000
              10 H      2.055200000000    1.524570000000    0.170250000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    11 
    Geometry Index:     2 
    Coordinates:
               0 C     -1.154542521531    0.225556083397   -0.019286488562
               1 C      0.366627601531    0.193558720073   -0.017771896877
               2 H     -1.541836507024    0.624012825326    0.933222536584
               3 H     -1.574068763296   -0.783481814387   -0.158713958089
               4 H     -1.534883689100    0.865675076335   -0.832426168671
               5 H      0.719892410771   -0.475463186612    0.786355835958
               6 C      0.968127859693    1.577948724202    0.172670301355
               7 H      0.727665947830   -0.236151834030   -0.968717020604
               8 H      0.644452742747    2.019749398586    1.129550744042
               9 H      0.655619583786    2.258214334539   -0.636822506631
              10 H      2.068895334594    1.536661672571    0.173538621495
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    11 
    Geometry Index:     3 
    Coordinates:
               0 C     -1.152920737674    0.223208759440   -0.019161859139
               1 C      0.366857036184    0.193265538580   -0.017968799074
               2 H     -1.541091266243    0.624380072044    0.933438262867
               3 H     -1.583348658210   -0.781729636425   -0.158932778232
               4 H     -1.534937753349    0.868169755235   -0.829529117670
               5 H      0.723180132837   -0.480054291023    0.783274344219
               6 C      0.969637607761    1.575679161451    0.172119837435
               7 H      0.730826062118   -0.240164474427   -0.967842039426
               8 H      0.640556698665    2.021054610009    1.127033702523
               9 H      0.656018462742    2.257420222201   -0.637542145981
              10 H      2.071172415168    1.545050282915    0.176710592479
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    11 
    Geometry Index:     4 
    Coordinates:
               0 C     -1.150362511213    0.222422542855   -0.018777352171
               1 C      0.367468680885    0.192301817538   -0.018242991245
               2 H     -1.538634902774    0.624978229197    0.933172711433
               3 H     -1.589091137806   -0.778193185219   -0.159154959053
               4 H     -1.533436297249    0.871214421705   -0.825576061090
               5 H      0.726070575045   -0.483826676229    0.779553784185
               6 C      0.969325033863    1.573242706138    0.171513077067
               7 H      0.733146646965   -0.243632031508   -0.966239421611
               8 H      0.635401922873    2.021505137521    1.123352588325
               9 H      0.655653810856    2.255109019284   -0.637970772095
              10 H      2.070408178555    1.551158018719    0.179969396256
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    11 
    Geometry Index:     5 
    Coordinates:
               0 C     -1.149636957735    0.222712259959   -0.018523439526
               1 C      0.367792900801    0.191781724733   -0.018368744334
               2 H     -1.537762871300    0.624919185142    0.933329502367
               3 H     -1.589472198436   -0.777073470843   -0.159414946856
               4 H     -1.532980346330    0.872363387721   -0.824153260919
               5 H      0.726844825571   -0.484682074443    0.778511093802
               6 C      0.968763416430    1.572766959190    0.171316897461
               7 H      0.733445715019   -0.244407808257   -0.965892242497
               8 H      0.633539181613    2.021812310942    1.122025796821
               9 H      0.655805812635    2.254208763956   -0.638431549761
              10 H      2.069610521731    1.551878761900    0.181200893443
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    11 
    Geometry Index:     6 
    Coordinates:
               0 C     -1.149455401239    0.222902870552   -0.018320498306
               1 C      0.367941749655    0.191552668569   -0.018460022119
               2 H     -1.537470861170    0.624571235876    0.933702875511
               3 H     -1.589339835234   -0.776753616689   -0.159691839266
               4 H     -1.533015420891    0.873009327210   -0.823359930745
               5 H      0.727209392306   -0.484992399105    0.778100341779
               6 C      0.968504828925    1.572712439407    0.171198414047
               7 H      0.733403675569   -0.244601705121   -0.965949502492
               8 H      0.632500846745    2.022249934667    1.121297415343
               9 H      0.656363135074    2.253790764375   -0.639042626237
              10 H      2.069307890261    1.551838480259    0.182125372485
