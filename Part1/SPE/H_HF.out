
                                 *****************
                                 * O   R   C   A *
                                 *****************

                                            #,                                       
                                            ###                                      
                                            ####                                     
                                            #####                                    
                                            ######                                   
                                           ########,                                 
                                     ,,################,,,,,                         
                               ,,#################################,,                 
                          ,,##########################################,,             
                       ,#########################################, ''#####,          
                    ,#############################################,,   '####,        
                  ,##################################################,,,,####,       
                ,###########''''           ''''###############################       
              ,#####''   ,,,,##########,,,,          '''####'''          '####       
            ,##' ,,,,###########################,,,                        '##       
           ' ,,###''''                  '''############,,,                           
         ,,##''                                '''############,,,,        ,,,,,,###''
      ,#''                                            '''#######################'''  
     '                                                          ''''####''''         
             ,#######,   #######,   ,#######,      ##                                
            ,#'     '#,  ##    ##  ,#'     '#,    #''#        ######   ,####,        
            ##       ##  ##   ,#'  ##            #'  '#       #        #'  '#        
            ##       ##  #######   ##           ,######,      #####,   #    #        
            '#,     ,#'  ##    ##  '#,     ,#' ,#      #,         ##   #,  ,#        
             '#######'   ##     ##  '#######'  #'      '#     #####' # '####'        



                  #######################################################
                  #                        -***-                        #
                  #          Department of theory and spectroscopy      #
                  #    Directorship and core code : Frank Neese         #
                  #        Max Planck Institute fuer Kohlenforschung    #
                  #                Kaiser Wilhelm Platz 1               #
                  #                 D-45470 Muelheim/Ruhr               #
                  #                      Germany                        #
                  #                                                     #
                  #                  All rights reserved                #
                  #                        -***-                        #
                  #######################################################


                         Program Version 5.0.1  -   Release   -


 With contributions from (in alphabetic order):
   Daniel Aravena         : Magnetic Suceptibility
   Michael Atanasov       : Ab Initio Ligand Field Theory (pilot matlab implementation)
   Alexander A. Auer      : GIAO ZORA, VPT2 properties, NMR spectrum
   Ute Becker             : Parallelization
   Giovanni Bistoni       : ED, misc. LED, open-shell LED, HFLD
   Martin Brehm           : Molecular dynamics
   Dmytro Bykov           : SCF Hessian
   Vijay G. Chilkuri      : MRCI spin determinant printing, contributions to CSF-ICE
   Dipayan Datta          : RHF DLPNO-CCSD density
   Achintya Kumar Dutta   : EOM-CC, STEOM-CC
   Dmitry Ganyushin       : Spin-Orbit,Spin-Spin,Magnetic field MRCI
   Miquel Garcia          : C-PCM and meta-GGA Hessian, CC/C-PCM, Gaussian charge scheme
   Yang Guo               : DLPNO-NEVPT2, F12-NEVPT2, CIM, IAO-localization
   Andreas Hansen         : Spin unrestricted coupled pair/coupled cluster methods
   Benjamin Helmich-Paris : MC-RPA, TRAH-SCF, COSX integrals
   Lee Huntington         : MR-EOM, pCC
   Robert Izsak           : Overlap fitted RIJCOSX, COSX-SCS-MP3, EOM
   Marcus Kettner         : VPT2
   Christian Kollmar      : KDIIS, OOCD, Brueckner-CCSD(T), CCSD density, CASPT2, CASPT2-K
   Simone Kossmann        : Meta GGA functionals, TD-DFT gradient, OOMP2, MP2 Hessian
   Martin Krupicka        : Initial AUTO-CI
   Lucas Lang             : DCDCAS
   Marvin Lechner         : AUTO-CI (C++ implementation), FIC-MRCC
   Dagmar Lenk            : GEPOL surface, SMD
   Dimitrios Liakos       : Extrapolation schemes; Compound Job, initial MDCI parallelization
   Dimitrios Manganas     : Further ROCIS development; embedding schemes
   Dimitrios Pantazis     : SARC Basis sets
   Anastasios Papadopoulos: AUTO-CI, single reference methods and gradients
   Taras Petrenko         : DFT Hessian,TD-DFT gradient, ASA, ECA, R-Raman, ABS, FL, XAS/XES, NRVS
   Peter Pinski           : DLPNO-MP2, DLPNO-MP2 Gradient
   Christoph Reimann      : Effective Core Potentials
   Marius Retegan         : Local ZFS, SOC
   Christoph Riplinger    : Optimizer, TS searches, QM/MM, DLPNO-CCSD(T), (RO)-DLPNO pert. Triples
   Tobias Risthaus        : Range-separated hybrids, TD-DFT gradient, RPA, STAB
   Michael Roemelt        : Original ROCIS implementation
   Masaaki Saitow         : Open-shell DLPNO-CCSD energy and density
   Barbara Sandhoefer     : DKH picture change effects
   Avijit Sen             : IP-ROCIS
   Kantharuban Sivalingam : CASSCF convergence, NEVPT2, FIC-MRCI
   Bernardo de Souza      : ESD, SOC TD-DFT
   Georgi Stoychev        : AutoAux, RI-MP2 NMR, DLPNO-MP2 response
   Willem Van den Heuvel  : Paramagnetic NMR
   Boris Wezisla          : Elementary symmetry handling
   Frank Wennmohs         : Technical directorship


 We gratefully acknowledge several colleagues who have allowed us to
 interface, adapt or use parts of their codes:
   Stefan Grimme, W. Hujo, H. Kruse, P. Pracht,  : VdW corrections, initial TS optimization,
                  C. Bannwarth, S. Ehlert          DFT functionals, gCP, sTDA/sTD-DF
   Ed Valeev, F. Pavosevic, A. Kumar             : LibInt (2-el integral package), F12 methods
   Garnet Chan, S. Sharma, J. Yang, R. Olivares  : DMRG
   Ulf Ekstrom                                   : XCFun DFT Library
   Mihaly Kallay                                 : mrcc  (arbitrary order and MRCC methods)
   Jiri Pittner, Ondrej Demel                    : Mk-CCSD
   Frank Weinhold                                : gennbo (NPA and NBO analysis)
   Christopher J. Cramer and Donald G. Truhlar   : smd solvation model
   Lars Goerigk                                  : TD-DFT with DH, B97 family of functionals
   V. Asgeirsson, H. Jonsson                     : NEB implementation
   FAccTs GmbH                                   : IRC, NEB, NEB-TS, DLPNO-Multilevel, CI-OPT
                                                   MM, QMMM, 2- and 3-layer-ONIOM, Crystal-QMMM,
                                                   LR-CPCM, SF, NACMEs, symmetry and pop. for TD-DFT,
                                                   nearIR, NL-DFT gradient (VV10), updates on ESD,
                                                   ML-optimized integration grids
   S Lehtola, MJT Oliveira, MAL Marques          : LibXC Library
   Liviu Ungur et al                             : ANISO software


 Your calculation uses the libint2 library for the computation of 2-el integrals
 For citations please refer to: http://libint.valeyev.net

 Your ORCA version has been built with support for libXC version: 5.1.0
 For citations please refer to: https://tddft.org/programs/libxc/

 This ORCA versions uses:
   CBLAS   interface :  Fast vector & matrix operations
   LAPACKE interface :  Fast linear algebra routines
   Shared memory     :  Shared parallel matrices


================================================================================

----- Orbital basis set information -----
Your calculation utilizes the basis: def2-TZVPP
   F. Weigend and R. Ahlrichs, Phys. Chem. Chem. Phys. 7, 3297 (2005).

================================================================================
                                        WARNINGS
                       Please study these warnings very carefully!
================================================================================


WARNING: your system is open-shell and RHF/RKS was chosen
  ===> : WILL SWITCH to UHF/UKS


INFO   : the flag for use of the SHARK integral package has been found!

================================================================================
                                       INPUT FILE
================================================================================
NAME = .\H_HF.inp
|  1> # avogadro generated ORCA input file 

|  2> # Basic Mode

|  3> # MQT Intensive - H

|  4> ! RHF def2-TZVPP

|  5> 

|  6> * xyz 0 2

|  7>    H        0.00000        0.00000        0.00000

|  8> *

|  9> 

| 10> 
| 11>                          ****END OF INPUT****
================================================================================

                       ****************************
                       * Single Point Calculation *
                       ****************************

---------------------------------
CARTESIAN COORDINATES (ANGSTROEM)
---------------------------------
  H      0.000000    0.000000    0.000000

----------------------------
CARTESIAN COORDINATES (A.U.)
----------------------------
  NO LB      ZA    FRAG     MASS         X           Y           Z
   0 H     1.0000    0     1.008    0.000000    0.000000    0.000000

--------------------------------
INTERNAL COORDINATES (ANGSTROEM)
--------------------------------
 H      0   0   0     0.000000000000     0.00000000     0.00000000

---------------------------
INTERNAL COORDINATES (A.U.)
---------------------------
 H      0   0   0     0.000000000000     0.00000000     0.00000000

---------------------
BASIS SET INFORMATION
---------------------
There are 1 groups of distinct atoms

 Group   1 Type H   : 5s2p1d contracted to 3s2p1d pattern {311/11/1}

Atom   0H    basis set group =>   1
------------------------------------------------------------------------------
                           ORCA GTO INTEGRAL CALCULATION
------------------------------------------------------------------------------
------------------------------------------------------------------------------
                   ___                                                        
                  /   \      - P O W E R E D   B Y -                         
                 /     \                                                     
                 |  |  |   _    _      __       _____    __    __             
                 |  |  |  | |  | |    /  \     |  _  \  |  |  /  |          
                  \  \/   | |  | |   /    \    | | | |  |  | /  /          
                 / \  \   | |__| |  /  /\  \   | |_| |  |  |/  /          
                |  |  |   |  __  | /  /__\  \  |    /   |      \           
                |  |  |   | |  | | |   __   |  |    \   |  |\   \          
                \     /   | |  | | |  |  |  |  | |\  \  |  | \   \       
                 \___/    |_|  |_| |__|  |__|  |_| \__\ |__|  \__/        
                                                                              
                      - O R C A' S   B I G   F R I E N D -                    
                                      &                                       
                       - I N T E G R A L  F E E D E R -                       
                                                                              
 v1 FN, 2020, v2 2021                                                         
------------------------------------------------------------------------------


Reading SHARK input file .\H_HF.SHARKINP.tmp ... ok
----------------------
SHARK INTEGRAL PACKAGE
----------------------

Number of atoms                             ...      1
Number of basis functions                   ...     14
Number of shells                            ...      6
Maximum angular momentum                    ...      2
Integral batch strategy                     ... SHARK/LIBINT Hybrid
RI-J (if used) integral strategy            ... SPLIT-RIJ (Revised 2003 algorithm where possible)
Printlevel                                  ...      1
Contraction scheme used                     ... SEGMENTED contraction
Coulomb Range Separation                    ... NOT USED
Exchange Range Separation                   ... NOT USED
Finite Nucleus Model                        ... NOT USED
Auxiliary Coulomb fitting basis             ... NOT available
Auxiliary J/K fitting basis                 ... NOT available
Auxiliary Correlation fitting basis         ... NOT available
Auxiliary 'external' fitting basis          ... NOT available
Integral threshold                          ...     1.000000e-10
Primitive cut-off                           ...     1.000000e-11
Primitive pair pre-selection threshold      ...     1.000000e-11

Calculating pre-screening integrals         ... done (  0.0 sec) Dimension = 6
Organizing shell pair data                  ... done (  0.0 sec)
Shell pair information
Total number of shell pairs                 ...        21
Shell pairs after pre-screening             ...        21
Total number of primitive shell pairs       ...        39
Primitive shell pairs kept                  ...        39
          la=0 lb=0:      6 shell pairs
          la=1 lb=0:      6 shell pairs
          la=1 lb=1:      3 shell pairs
          la=2 lb=0:      3 shell pairs
          la=2 lb=1:      2 shell pairs
          la=2 lb=2:      1 shell pairs

Calculating one electron integrals          ... done (  0.0 sec)
Calculating Nuclear repulsion               ... done (  0.0 sec) ENN=      0.000000000000 Eh

SHARK setup successfully completed in   0.0 seconds

Maximum memory used throughout the entire GTOINT-calculation: 6.0 MB
-------------------------------------------------------------------------------
                                 ORCA SCF
-------------------------------------------------------------------------------

------------
SCF SETTINGS
------------
Hamiltonian:
 Ab initio Hamiltonian  Method          .... Hartree-Fock(GTOs)


General Settings:
 Integral files         IntName         .... .\H_HF
 Hartree-Fock type      HFTyp           .... UHF
 Total Charge           Charge          ....    0
 Multiplicity           Mult            ....    2
 Number of Electrons    NEL             ....    1
 Basis Dimension        Dim             ....   14
 Nuclear Repulsion      ENuc            ....      0.0000000000 Eh

Convergence Acceleration:
 DIIS                   CNVDIIS         .... on
   Start iteration      DIISMaxIt       ....    12
   Startup error        DIISStart       ....  0.200000
   # of expansion vecs  DIISMaxEq       ....     5
   Bias factor          DIISBfac        ....   1.050
   Max. coefficient     DIISMaxC        ....  10.000
 Trust-Rad. Augm. Hess. CNVTRAH         .... auto
   Auto Start mean grad. ratio tolernc. ....  1.125000
   Auto Start start iteration           ....    20
   Auto Start num. interpolation iter.  ....    10
   Max. Number of Micro iterations      ....    16
   Max. Number of Macro iterations      .... Maxiter - #DIIS iter
   Number of Davidson start vectors     ....     2
   Converg. threshold I  (grad. norm)   ....   5.000e-05
   Converg. threshold II (energy diff.) ....   1.000e-06
   Grad. Scal. Fac. for Micro threshold ....   0.100
   Minimum threshold for Micro iter.    ....   0.010
   NR start threshold (gradient norm)   ....   0.001
   Initial trust radius                 ....   0.400
   Minimum AH scaling param. (alpha)    ....   1.000
   Maximum AH scaling param. (alpha)    .... 1000.000
   Orbital update algorithm             .... Taylor
   White noise on init. David. guess    .... on
   Maximum white noise                  ....   0.010
   Quad. conv. algorithm                .... NR
 SOSCF                  CNVSOSCF        .... off
 Level Shifting         CNVShift        .... on
   Level shift para.    LevelShift      ....    0.2500
   Turn off err/grad.   ShiftErr        ....    0.0010
 Zerner damping         CNVZerner       .... off
 Static damping         CNVDamp         .... on
   Fraction old density DampFac         ....    0.7000
   Max. Damping (<1)    DampMax         ....    0.9800
   Min. Damping (>=0)   DampMin         ....    0.0000
   Turn off err/grad.   DampErr         ....    0.1000
 Fernandez-Rico         CNVRico         .... off

SCF Procedure:
 Maximum # iterations   MaxIter         ....   125
 SCF integral mode      SCFMode         .... Direct
   Integral package                     .... SHARK and LIBINT hybrid scheme
 Reset frequency        DirectResetFreq ....    20
 Integral Threshold     Thresh          ....  1.000e-10 Eh
 Primitive CutOff       TCut            ....  1.000e-11 Eh

Convergence Tolerance:
 Convergence Check Mode ConvCheckMode   .... Total+1el-Energy
 Convergence forced     ConvForced      .... 0
 Energy Change          TolE            ....  1.000e-06 Eh
 1-El. energy change                    ....  1.000e-03 Eh
 DIIS Error             TolErr          ....  1.000e-06


Diagonalization of the overlap matrix:
Smallest eigenvalue                        ... 1.094e-01
Time for diagonalization                   ...    0.000 sec
Threshold for overlap eigenvalues          ... 1.000e-08
Number of eigenvalues below threshold      ... 0
Time for construction of square roots      ...    0.001 sec
Total time needed                          ...    0.001 sec

Time for model grid setup =    0.001 sec

------------------------------
INITIAL GUESS: MODEL POTENTIAL
------------------------------
Loading Hartree-Fock densities                     ... done
Calculating cut-offs                               ... done
Initializing the effective Hamiltonian             ... done
Setting up the integral package (SHARK)            ... done
Starting the Coulomb interaction                   ... done (   0.0 sec)
Reading the grid                                   ... done
Mapping shells                                     ... done
Starting the XC term evaluation                    ... done (   0.0 sec)
Transforming the Hamiltonian                       ... done (   0.0 sec)
Diagonalizing the Hamiltonian                      ... done (   0.0 sec)
Back transforming the eigenvectors                 ... done (   0.0 sec)
Now organizing SCF variables                       ... done
                      ------------------
                      INITIAL GUESS DONE (   0.0 sec)
                      ------------------
--------------
SCF ITERATIONS
--------------
ITER       Energy         Delta-E        Max-DP      RMS-DP      [F,P]     Damp
               ***  Starting incremental Fock matrix formation  ***
  0     -0.4951036604   0.000000000000 0.01896118  0.00115801  0.0436791 0.7000
  1     -0.4959863884  -0.000882727921 0.01893520  0.00118185  0.0373139 0.7000
                               ***Turning on DIIS***
  2     -0.4967904833  -0.000804094917 0.05349138  0.00338176  0.0310333 0.0000
  3     -0.4996340060  -0.002843522731 0.02473515  0.00171058  0.0130770 0.0000
  4     -0.4998366088  -0.000202602821 0.00633260  0.00045842  0.0030571 0.0000
  5     -0.4998175067   0.000019102179 0.00232820  0.00016204  0.0007062 0.0000
  6     -0.4998095361   0.000007970596 0.00019242  0.00001273  0.0000475 0.0000
                 **** Energy Check signals convergence ****

               *****************************************************
               *                     SUCCESS                       *
               *           SCF CONVERGED AFTER   7 CYCLES          *
               *****************************************************


----------------
TOTAL SCF ENERGY
----------------

Total Energy       :           -0.49980982 Eh             -13.60052 eV

Components:
Nuclear Repulsion  :            0.00000000 Eh               0.00000 eV
Electronic Energy  :           -0.49980982 Eh             -13.60052 eV
One Electron Energy:           -0.49980983 Eh             -13.60052 eV
Two Electron Energy:            0.00000001 Eh               0.00000 eV

Virial components:
Potential Energy   :           -0.99961940 Eh             -27.20103 eV
Kinetic Energy     :            0.49980958 Eh              13.60051 eV
Virial Ratio       :            2.00000049


---------------
SCF CONVERGENCE
---------------

  Last Energy change         ...   -2.8651e-07  Tolerance :   1.0000e-06
  Last MAX-Density change    ...    2.1818e-05  Tolerance :   1.0000e-05
  Last RMS-Density change    ...    1.4270e-06  Tolerance :   1.0000e-06
  Last DIIS Error            ...    4.8385e-06  Tolerance :   1.0000e-06

             **** THE GBW FILE WAS UPDATED (.\H_HF.gbw) ****
             **** DENSITY .\H_HF.scfp WAS UPDATED ****
             **** ENERGY FILE WAS UPDATED (.\H_HF.en.tmp) ****
----------------------
UHF SPIN CONTAMINATION
----------------------

Expectation value of <S**2>     :     0.750000
Ideal value S*(S+1) for S=0.5   :     0.750000
Deviation                       :     0.000000

             **** THE GBW FILE WAS UPDATED (.\H_HF.gbw) ****
             **** DENSITY .\H_HF.scfp WAS UPDATED ****
----------------
ORBITAL ENERGIES
----------------
                 SPIN UP ORBITALS
  NO   OCC          E(Eh)            E(eV) 
   0   1.0000      -0.499810       -13.6005 
   1   0.0000       0.350640         9.5414 
   2   0.0000       0.737500        20.0684 
   3   0.0000       0.737500        20.0684 
   4   0.0000       0.737500        20.0684 
   5   0.0000       2.482423        67.5502 
   6   0.0000       3.429922        93.3329 
   7   0.0000       3.429922        93.3329 
   8   0.0000       3.429922        93.3329 
   9   0.0000       3.429922        93.3329 
  10   0.0000       3.429922        93.3329 
  11   0.0000       3.846040       104.6561 
  12   0.0000       3.846040       104.6561 
  13   0.0000       3.846040       104.6561 

                 SPIN DOWN ORBITALS
  NO   OCC          E(Eh)            E(eV) 
   0   0.0000       0.056440         1.5358 
   1   0.0000       0.494332        13.4515 
   2   0.0000       0.835391        22.7321 
   3   0.0000       0.835391        22.7321 
   4   0.0000       0.835391        22.7321 
   5   0.0000       2.613788        71.1248 
   6   0.0000       3.486507        94.8727 
   7   0.0000       3.486507        94.8727 
   8   0.0000       3.486507        94.8727 
   9   0.0000       3.486507        94.8727 
  10   0.0000       3.486507        94.8727 
  11   0.0000       3.933624       107.0394 
  12   0.0000       3.933624       107.0394 
  13   0.0000       3.933624       107.0394 

                    ********************************
                    * MULLIKEN POPULATION ANALYSIS *
                    ********************************

--------------------------------------------
MULLIKEN ATOMIC CHARGES AND SPIN POPULATIONS
--------------------------------------------
   0 H :    0.000000    1.000000
Sum of atomic charges         :    0.0000000
Sum of atomic spin populations:    1.0000000

-----------------------------------------------------
MULLIKEN REDUCED ORBITAL CHARGES AND SPIN POPULATIONS
-----------------------------------------------------
CHARGE
  0 H s       :     1.000000  s :     1.000000
      pz      :     0.000000  p :     0.000000
      px      :     0.000000
      py      :     0.000000
      dz2     :     0.000000  d :     0.000000
      dxz     :     0.000000
      dyz     :     0.000000
      dx2y2   :     0.000000
      dxy     :     0.000000

SPIN
  0 H s       :     1.000000  s :     1.000000
      pz      :     0.000000  p :     0.000000
      px      :     0.000000
      py      :     0.000000
      dz2     :     0.000000  d :     0.000000
      dxz     :     0.000000
      dyz     :     0.000000
      dx2y2   :     0.000000
      dxy     :     0.000000


                     *******************************
                     * LOEWDIN POPULATION ANALYSIS *
                     *******************************

-------------------------------------------
LOEWDIN ATOMIC CHARGES AND SPIN POPULATIONS
-------------------------------------------
   0 H :    0.000000    1.000000

----------------------------------------------------
LOEWDIN REDUCED ORBITAL CHARGES AND SPIN POPULATIONS
----------------------------------------------------
CHARGE
  0 H s       :     1.000000  s :     1.000000
      pz      :     0.000000  p :     0.000000
      px      :     0.000000
      py      :     0.000000
      dz2     :     0.000000  d :     0.000000
      dxz     :     0.000000
      dyz     :     0.000000
      dx2y2   :     0.000000
      dxy     :     0.000000

SPIN
  0 H s       :     1.000000  s :     1.000000
      pz      :     0.000000  p :     0.000000
      px      :     0.000000
      py      :     0.000000
      dz2     :     0.000000  d :     0.000000
      dxz     :     0.000000
      dyz     :     0.000000
      dx2y2   :     0.000000
      dxy     :     0.000000


                      *****************************
                      * MAYER POPULATION ANALYSIS *
                      *****************************

  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence

  ATOM       NA         ZA         QA         VA         BVA        FA
  0 H      1.0000     1.0000     0.0000     1.0000     0.0000     1.0000

  Mayer bond orders larger than 0.100000


Local spin analysis not attempted because no beta electrons available

Warning: MO coefficients are modified such that the real solid harmonic m components
have the correct order within each shell (0, +1, -1, +2, -2, ...) and all s orbitals
come before all p orbitals etc. (with increasing orbital energy within each block of given angular momentum).

--------------------------
ATOM BASIS FOR ELEMENT H 
--------------------------
 NewGTO H 
 S 5
    1         34.061341000000         0.006025199298
    2          5.123574600000         0.045021105197
    3          1.164662600000         0.201897310211
    4          0.327230410000         0.503040445218
    5          0.103072410000         0.385457994234
 S 5
    1         34.061341000000         0.002421589909
    2          5.123574600000         0.018094447771
    3          1.164662600000         0.081144616926
    4          0.327230410000         1.318913158400
    5          0.103072410000        -1.556495776970
 S 5
    1         34.061341000000         0.039095966809
    2          5.123574600000         0.292130359064
    3          1.164662600000         1.310059659096
    4          0.327230410000        -1.879578194480
    5          0.103072410000         0.804251745909
 P 2
    1          1.407000000000        -0.093233143619
    2          0.388000000000         1.054624821544
 P 2
    1          1.407000000000         1.264659349406
    2          0.388000000000        -0.704146415806
 D 1
    1          1.057000000000         1.000000000000
 end
-------------------------------------------
RADIAL EXPECTATION VALUES <R**-3> TO <R**3>
-------------------------------------------
   0 :     0.000000     1.986729     0.999619     1.499035     2.990530     7.426514
   1 :     0.000000     0.887273     0.565654     2.933426    10.376027    39.985273
   2 :     0.000000     9.105446     1.922798     1.261549     2.936362     9.465913
   3 :     0.384043     0.446994     0.626133     1.771588     3.422528     7.125117
   4 :     0.384043     0.446994     0.626133     1.771588     3.422528     7.125117
   5 :     0.384043     0.446994     0.626133     1.771588     3.422528     7.125117
   6 :     3.953005     1.946339     1.243022     1.051402     1.492936     2.747567
   7 :     3.953005     1.946339     1.243022     1.051402     1.492936     2.747567
   8 :     3.953005     1.946339     1.243022     1.051402     1.492936     2.747567
   9 :     0.924871     0.845600     0.874996     1.241717     1.655629    11.747557
  10 :     0.924871     0.845600     0.874996     1.241717     1.655629    11.747557
  11 :     0.924871     0.845600     0.874996     1.241717     1.655629    11.747557
  12 :     0.924871     0.845600     0.874996     1.241717     1.655629    11.747557
  13 :     0.924871     0.845600     0.874996     1.241717     1.655629    11.747557
-------
TIMINGS
-------

Total SCF time: 0 days 0 hours 0 min 0 sec 

Total time                  ....       0.174 sec
Sum of individual times     ....       0.073 sec  ( 42.0%)

Fock matrix formation       ....       0.040 sec  ( 23.0%)
Diagonalization             ....       0.001 sec  (  0.6%)
Density matrix formation    ....       0.000 sec  (  0.0%)
Population analysis         ....       0.005 sec  (  2.9%)
Initial guess               ....       0.007 sec  (  4.0%)
Orbital Transformation      ....       0.000 sec  (  0.0%)
Orbital Orthonormalization  ....       0.000 sec  (  0.0%)
DIIS solution               ....       0.019 sec  ( 10.9%)

Maximum memory used throughout the entire SCF-calculation: 224.0 MB

-------------------------   --------------------
FINAL SINGLE POINT ENERGY        -0.499809822571
-------------------------   --------------------


                            ***************************************
                            *     ORCA property calculations      *
                            ***************************************

                                    ---------------------
                                    Active property flags
                                    ---------------------
   (+) Dipole Moment


------------------------------------------------------------------------------
                       ORCA ELECTRIC PROPERTIES CALCULATION
------------------------------------------------------------------------------

Dipole Moment Calculation                       ... on
Quadrupole Moment Calculation                   ... off
Polarizability Calculation                      ... off
GBWName                                         ... .\H_HF.gbw
Electron density                                ... .\H_HF.scfp
The origin for moment calculation is the CENTER OF MASS  = ( 0.000000,  0.000000  0.000000)

-------------
DIPOLE MOMENT
-------------
                                X             Y             Z
Electronic contribution:     -0.00000      -0.00000      -0.00000
Nuclear contribution   :      0.00000       0.00000       0.00000
                        -----------------------------------------
Total Dipole Moment    :     -0.00000      -0.00000      -0.00000
                        -----------------------------------------
Magnitude (a.u.)       :      0.00000
Magnitude (Debye)      :      0.00000



--------------------
Rotational spectrum 
--------------------
 
Rotational constants in cm-1:     0.000000     0.000000     0.000000 
Rotational constants in MHz :     0.000000     0.000000     0.000000 

 Dipole components along the rotational axes: 
x,y,z [a.u.] :    -0.000000    -0.000000    -0.000000 
x,y,z [Debye]:    -0.000000    -0.000000    -0.000000 

 

Timings for individual modules:

Sum of individual times         ...        1.557 sec (=   0.026 min)
GTO integral calculation        ...        1.369 sec (=   0.023 min)  87.9 %
SCF iterations                  ...        0.188 sec (=   0.003 min)  12.1 %
                             ****ORCA TERMINATED NORMALLY****
TOTAL RUN TIME: 0 days 0 hours 0 minutes 1 seconds 619 msec
