import os, glob, csv
from matplotlib import pyplot as plt

here = os.getcwd()
outputs = glob.glob("./*/*.relaxscanscf.dat")
names = []

functionals = {"F1":"M06", "F2":"B3LYP", "F3":"B3LYP-D3"}
referenceX = [i/10 for i in range(30,61)]
referenceY = [2.24, 1.21, 0.57, 0.18, -0.06, -0.19, -0.26, -0.28, -0.28, -0.27,
			-0.25, -0.23, -0.21, -0.18, -0.16, -0.14, -0.13, -0.11, -0.10, -0.09,
			-0.08, -0.07, -0.06, -0.05, -0.05, -0.04, -0.04, -0.03, -0.03, -0.03, -0.02]

colors = ["b", "r", "y"]
symbol = ["s", "^", "v"]

for key, functional in functionals.items():
	baseline = abs(sum(referenceY[-4:-1]))/3
	referenceY = [i + baseline for i in referenceY]
	plt.plot(referenceX, referenceY, "go", label="Reference values")
	for out in outputs:
		if key in out:
			proto_name = out.split(".")
			name = proto_name[1].split("\\")
			names.append(name[2])
			full_path = os.path.join(here, out)
			tempx, tempy = [], []
			with open(full_path, "r") as f:
				data = csv.reader(f, delimiter=' ')
				for row in data:
					tempx.append(float(row[3]))
					tempy.append(float(row[4]) * 627.5)
			grid = int(name[-1][-1])
			baseline = abs(sum(tempy[-4:-1]))/3
			tempy = [i + baseline for i in tempy]
			plt.plot(tempx, tempy, f"{colors[grid - 1]}{symbol[grid - 1]}", label=f"DEFGRID{grid}")

	plt.title(f"DFT calculation of Argon dimer PES\n using the {functional} functional")
	plt.xlabel(r"Internuclear distance / $\AA$")
	plt.ylabel(r"Energy / $kcal\ mol^{-1}$")
	plt.legend()
	plt.savefig(f"gridArAr{functional}.png", dpi=300)
	plt.cla()