import os

here = os.getcwd()
grids = [1,3,5,7]
functionals = ["B3LYP", "M06", "B3LYP-D3"]

template = """# Input generated with Rony's script
! {func} def2-QZVPP Opt
%method
	AngularGrid {grid}
	IntAcc 5
end
%geom Scan
   B 0 1 = 3.0, 6.0, 31
end
end


* xyz 0 1
  Ar        0.00000        0.00000        0.00000
  Ar        3.00000        0.00000        0.00000
*
"""

for f in functionals:
	for g in grids:
		name = f"ArAr_{f}_{g}"
		print(name, end=" - ")
		os.mkdir(name)
		print("dir created!")
		os.chdir(os.path.join(here, name))
		with open(name + ".inp", "w") as h:
			h.write(template.format(func=f.replace("-", " "), grid=g))
		print("Input created!\nRunning Orca ... ", end="")
		os.system(f"orca.exe {name}.inp > {name}.out")
		print("... calculation over!")
		os.chdir(here)