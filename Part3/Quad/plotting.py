import os, glob, csv
from matplotlib import pyplot as plt

here = os.getcwd()
outputs = glob.glob("./*/*/*.relaxscanscf.dat")
names = []

functionals = ["HF", "B3LYP", "PW91"]

for functional in functionals:
	for out in outputs:
		if functional in out:
			proto_name = out.split(".")
			name = proto_name[1].split("\\")
			names.append(name[2])
			full_path = os.path.join(here, out)
			tempx, tempy = [], []
			with open(full_path, "r") as f:
				data = csv.reader(f, delimiter=' ')
				for row in data:
					hell = [float(i) for i in row if i != '']
					tempx.append(float(hell[0]))
					tempy.append(float(hell[1]) * 2625.5)
			if "Unrestricted" in out:
				sl = "U"
				m = "^-"
			else:
				sl = "R"
				m = "o-"
			if "HF" in out:
				theo = "HF"
			else:
				theo = "KS"
			baseline = abs(min(tempy))
			tempy = [i + baseline for i in tempy]
			plt.plot(tempx[2:], tempy[2:], m, label=f"{sl}{theo} {functional}")

plt.title(f"Dissociation of the Hydrogen Molecule")
plt.xlabel(r"Internuclear distance / $\AA$")
plt.ylabel(r"Energy / $kJ\ mol^{-1}$")
plt.legend()
plt.savefig(f"HH.png", dpi=300)
plt.cla()