-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -150.3274532415
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons         7.9999993380 
   Number of Alpha Electrons         7.9999993380 
   Total nuber of  Electrons        15.9999986760 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -12.0435277295 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0000001597
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       2
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             62
     number of aux C basis functions:       0
     number of aux J basis functions:       98
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -150.327453
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : .\O2_UKS_s_stab_v2.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000058569
        Electronic Contribution:
                  0    
      0       0.000002
      1      -0.000000
      2       0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0       0.000002
      1      -0.000000
      2       0.000000
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    2 
    Geometry Index:     1 
    Coordinates:
               0 O      0.000000000000    0.000000000000    0.000000000000
               1 O      1.210000000000    0.000000000000    0.000000000000
