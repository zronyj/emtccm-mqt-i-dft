-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -225.3934341092
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons        12.9999993404 
   Number of Alpha Electrons        10.9999999157 
   Total nuber of  Electrons        23.9999992561 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -18.1011215933 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0000172255
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -225.4015217382
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons        12.9999951815 
   Number of Alpha Electrons        10.9999973525 
   Total nuber of  Electrons        23.9999925340 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -18.0797096223 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0000272050
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -225.4019971474
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons        12.9999943507 
   Number of Alpha Electrons        10.9999968621 
   Total nuber of  Electrons        23.9999912128 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -18.0873685754 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0000276489
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -225.4020663942
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons        12.9999940968 
   Number of Alpha Electrons        10.9999967463 
   Total nuber of  Electrons        23.9999908431 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -18.0926286433 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0000275873
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -225.4020665715
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons        12.9999941105 
   Number of Alpha Electrons        10.9999967550 
   Total nuber of  Electrons        23.9999908655 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -18.0926827479 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0000275478
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 5
   prop. index: 1
       Filename                          : .\O3_RKS_t.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.3063228291
        Electronic Contribution:
                  0    
      0       0.063101
      1      -0.102674
      2       0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1      -0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0       0.063101
      1      -0.102674
      2       0.000000
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 O      0.000000000000    0.000000000000    0.000000000000
               1 O      1.272000000000    0.000000000000    0.000000000000
               2 O      1.845516231757    1.135369161071    0.000000000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     2 
    Coordinates:
               0 O     -0.039712673054   -0.060657628025    0.000000000000
               1 O      1.239661891235    0.052553702447    0.000000000000
               2 O      1.917567013575    1.143473086649    0.000000000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     3 
    Coordinates:
               0 O     -0.037904023288   -0.068420573844    0.000000000000
               1 O      1.231725776303    0.065408665467    0.000000000000
               2 O      1.923694478742    1.138381069447    0.000000000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     4 
    Coordinates:
               0 O     -0.035751698739   -0.071418308483    0.000000000000
               1 O      1.227859653624    0.071665505727    0.000000000000
               2 O      1.925408276872    1.135121963827    0.000000000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     5 
    Coordinates:
               0 O     -0.035636430394   -0.071196281165    0.000000000000
               1 O      1.227993531231    0.071444758129    0.000000000000
               2 O      1.925159130920    1.135120684107    0.000000000000
