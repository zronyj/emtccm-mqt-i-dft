-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -225.4020665046
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons        12.9999941105 
   Number of Alpha Electrons        10.9999967550 
   Total nuber of  Electrons        23.9999908655 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -18.0926824874 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0000275478
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          3
     Charge:                                0
     number of atoms:                       3
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             93
     number of aux C basis functions:       0
     number of aux J basis functions:       147
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -225.402094
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : o3ukstsa.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.3063041487
        Electronic Contribution:
                  0    
      0       0.063096
      1      -0.102669
      2       0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1      -0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0       0.063096
      1      -0.102669
      2       0.000000
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 O     -0.035636430394   -0.071196281165    0.000000000000
               1 O      1.227993531231    0.071444758129    0.000000000000
               2 O      1.925159130920    1.135120684107    0.000000000000
