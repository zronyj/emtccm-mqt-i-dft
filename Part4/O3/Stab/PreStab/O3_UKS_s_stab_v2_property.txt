-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -225.4454653436
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons        11.9999994625 
   Number of Alpha Electrons        11.9999990452 
   Total nuber of  Electrons        23.9999985077 
   Exchange energy                   0.0000000000 
   Correlation energy                0.0000000000 
   Correlantion energy NL            0.0000000000 
   Exchange-Correlation energy     -18.0900307886 
   Embedding correction              0.0000000000 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0000172255
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       3
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             93
     number of aux C basis functions:       0
     number of aux J basis functions:       147
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -225.445483
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : .\O3_UKS_s_stab_v2.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.5370679258
        Electronic Contribution:
                  0    
      0       0.110754
      1      -0.179942
      2      -0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1      -0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0       0.110754
      1      -0.179942
      2      -0.000000
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 O      0.000000000000    0.000000000000    0.000000000000
               1 O      1.272000000000    0.000000000000    0.000000000000
               2 O      1.845516231757    1.135369161071    0.000000000000
